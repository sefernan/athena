
#include "../TrigEFCaloHypoNoise.h"
#include "../TrigL2JetHypo.h"
#include "../TrigLArNoiseBurstHypoToolInc.h"
#include "../TrigLArNoiseBurstAlg.h"

DECLARE_COMPONENT( TrigEFCaloHypoNoise )
DECLARE_COMPONENT( TrigLArNoiseBurstHypoToolInc )
DECLARE_COMPONENT( TrigLArNoiseBurstAlg )
DECLARE_COMPONENT( TrigL2JetHypo )

